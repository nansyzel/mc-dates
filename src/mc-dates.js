var mcDateInputFormat = 'YYYY-MM-DD';
var mcDatesApp = angular.module('mcDates', ['ngMaterial', 'ngMessages']);
mcDatesApp.config(function ($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function (date) {
        return moment(date).format('DD.MM.YYYY');
    };
});
var mcDatesController = function ($scope, $timeout) {
    var dates = this;
    var prevDateFrom = null;
    var prevDateFromDt = null;
    var prevDateTo = null;
    var prevDateToDt = null;
    dates.change = function () {
        $timeout(dates.mcChange, 500);
    };
    dates.$doCheck = function () {
        if (!((prevDateFrom === dates.dateFrom)
            && (prevDateTo === dates.dateTo)
            && (moment(prevDateFromDt).isSame(dates.dateFromDt))
            && (moment(prevDateToDt).isSame(dates.dateToDt)))) {
            if (prevDateFrom !== dates.dateFrom) {
                ;
                if (moment(dates.dateFrom, mcDateInputFormat, true).isValid()) {
                    dates.dateFromDt = moment(dates.dateFrom, mcDateInputFormat).toDate();
                }
                else {
                    dates.dateFromDt = null;
                }
                ;
                prevDateFrom = dates.dateFrom;
                prevDateFromDt = dates.dateFromDt;
            }
            else if (prevDateFromDt != dates.dateFromDt) {
                if (moment(dates.dateFromDt).isValid()) {
                    dates.dateFrom = moment(dates.dateFromDt).format(mcDateInputFormat);
                }
                else {
                    dates.dateFrom = null;
                }
                dates.isDateFromVlaid = true;
                prevDateFrom = dates.dateFrom;
                prevDateFromDt = dates.dateFromDt;
            }
            if (prevDateTo !== dates.dateTo) {
                if (moment(dates.dateTo, mcDateInputFormat, true).isValid()) {
                    dates.dateToDt = moment(dates.dateTo, mcDateInputFormat).toDate();
                }
                else {
                    dates.dateToDt = null;
                }
                ;
                prevDateTo = dates.dateTo;
                prevDateToDt = dates.dateToDt;
            }
            else if (prevDateToDt != dates.dateToDt) {
                if ((dates.dateToDt === null) || (dates.dateToDt === undefined)) {
                    dates.dateTo = null;
                }
                else {
                    dates.dateTo = moment(dates.dateToDt).format(mcDateInputFormat);
                }
                ;
                prevDateTo = dates.dateTo;
                prevDateToDt = dates.dateToDt;
            }
        }
        ;
    };
    dates.setYesterday = function () {
        var yesterday = moment().subtract('days', 1).toDate();
        dates.dateFromDt = yesterday;
        dates.dateToDt = yesterday;
        dates.change();
    };
    dates.setToday = function () {
        var today = new Date();
        dates.dateFromDt = today;
        dates.dateToDt = today;
        dates.change();
    };
    dates.setTwoWeeks = function () {
        var today = new Date();
        var twoWeeksAgo = moment().subtract('days', 14).toDate();
        dates.dateFromDt = twoWeeksAgo;
        dates.dateToDt = today;
        dates.change();
    };
    dates.setMonth = function () {
        var today = new Date();
        var monthAgo = moment().subtract('days', 30).toDate();
        dates.dateFromDt = monthAgo;
        dates.dateToDt = today;
        dates.change();
    };
    dates.setAll = function () {
        dates.dateFromDt = null;
        dates.dateToDt = null;
        dates.change();
    };
};
// component
mcDatesApp.component('mcDates', {
    bindings: {
        'dateFrom': '=',
        'dateTo': '=',
        'mcChange': '&'
    },
    templateUrl: 'html/mc-dates-component.html',
    controller: mcDatesController,
    controllerAs: 'dates'
});
